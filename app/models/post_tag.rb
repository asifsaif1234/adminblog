class PostTag < ActiveRecord::Base
  belongs_to :post
  has_many :tags, through: :post_tags
  belongs_to :tag
end
