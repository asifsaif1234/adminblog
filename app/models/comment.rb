class Comment < ActiveRecord::Base
  belongs_to :post
  has_many :notifications, as: :notifiable, dependent: :destroy
  belongs_to :visitor

  def self.fullname_message params
  	joins(:visitor).where("fullname LIKE ? OR message LIKE ?", "%#{params}%", "%#{params}%")
  end
end
