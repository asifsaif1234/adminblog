class Admin::CommentsController < Admin::ApplicationController
  def index
  	if params[:search].present?
  		@comments = Comment.fullname_message(params[:search]).page(params[:page]).per(3)
  	else
  		@comments = Comment.where(status: to_bool(params[:status])).page params[:page]

  	end
  end

  def update
  	@comment = Comment.find(params[:id])
  	if @comment.update(status: params[:status])
  		redirect_to :back, notice: 'Comment updated successfully'
  	else
  		redirect_to :back, notice: 'There was a problem in updating comment'
  	end
  end

  def destroy
  	@comment = Comment.find(params[:id])
  	@comment.destroy

  	redirect_to :back, notice: "Comment deleted succesfully"
  end
end
